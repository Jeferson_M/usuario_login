﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Usuario.Log
{
    class Usuario
    {

        private int id;
        private string nombre_usuario;
        private string contraseña;
        private string confirm_contraseña;
        private string correo;
        private string confirm_correo;
        private string telefono;
        private string confir_telefono;
        private int fallidos;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nombre_usuario
        {
            get
            {
                return nombre_usuario;
            }

            set
            {
                nombre_usuario = value;
            }
        }

        public string Contraseña
        {
            get
            {
                return contraseña;
            }

            set
            {
                contraseña = value;
            }
        }

        public string Confirm_contraseña
        {
            get
            {
                return confirm_contraseña;
            }

            set
            {
                confirm_contraseña = value;
            }
        }

        public string Correo
        {
            get
            {
                return correo;
            }

            set
            {
                correo = value;
            }
        }

        public string Confirm_correo
        {
            get
            {
                return confirm_correo;
            }

            set
            {
                confirm_correo = value;
            }
        }

        public string Telefono
        {
            get
            {
                return telefono;
            }

            set
            {
                telefono = value;
            }
        }

        public string Confir_telefono
        {
            get
            {
                return confir_telefono;
            }

            set
            {
                confir_telefono = value;
            }
        }

        public int Fallidos
        {
            get
            {
                return fallidos;
            }

            set
            {
                fallidos = value;
            }
        }

        public Usuario(int id, string nombre_usuario, string contraseña, string confirm_contraseña, string correo, string confirm_correo, string telefono, string confir_telefono, int fallidos)
        {
            this.Id = id;
            this.Nombre_usuario = nombre_usuario;
            this.Contraseña = contraseña;
            this.Confirm_contraseña = confirm_contraseña;
            this.Correo = correo;
            this.Confirm_correo = confirm_correo;
            this.Telefono = telefono;
            this.Confir_telefono = confir_telefono;
            this.Fallidos = fallidos;
        }
    }
}
