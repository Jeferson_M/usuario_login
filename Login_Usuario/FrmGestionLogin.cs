﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_Usuario
{
    public partial class FrmGestionLogin : Form
    {

        private DataSet dsLog;
        private BindingSource bsLog;

        public DataSet DsLog { set { dsLog = value; } }
        public FrmGestionLogin()
        {
            InitializeComponent();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmLoginUsuario fl = new FrmLoginUsuario();
            fl.TblLog = dsLog.Tables["Usuario"];
            fl.DsLog = dsLog;
            fl.ShowDialog();
        }
    }
}
